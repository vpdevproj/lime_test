<?php

namespace App;

use Illuminate\Support\Facades\Log;


class limeGraph
{
    public function _construct(){
        //
    }

    private function createQuery($body){
        try{
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,'https://graphql.diamondcasino.africa/graphql');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($body)
            ));

            $resp = curl_exec($ch);
            curl_close ($ch);

            return $resp;
        }catch(\Exception $e){
            Log::error($e);
            return false;
        };
    }

    private function popularGame($type = 'DESKTOP',$size = 9){
        $tGames = array();
        $topGames = array(
            'query' => 'query{
                popularGame(type: '.$type.',brandId: ""){
                    gameId
                    fullGameName
                    metaTitle
                    metaDescription
                    category
                    image
                    description
                    playersOnline
                }
            }'
        );
        $j = 0;
        $gamesArr = array();
        for($i = 0; $i < 100; $i++){
            if($j < (int)$size){
                $obj = $this->createQuery($topGames);
                if($obj){
                    $obj = json_decode($obj,true);
                    if(!in_array($obj['data']['popularGame']['gameId'],$gamesArr)){
                        $tGames[] = $obj['data']['popularGame'];
                        $gamesArr[] = $obj['data']['popularGame']['gameId'];
                        $j++;
                    };
                };
            }else{
                break;
            };
        };

        return $tGames;
    }

    private function topWinnersJackpot($wsize = 5){
        $answers = array();
        $Winners_JackPots = 'query{
            topWinners(brandId:"",size:'.(int)$wsize.'){
                uuid
                username
                game{
                    gameId
                    fullGameName
                    metaTitle
                    metaDescription
                    category
                    image
                    description
                    playersOnline
                }
                date
                winAmount{
                    amount
                    currency
                }
	        }
	        jackpot(locale:""){
                amount
                currency
            }
        }';

        $wj = $this->createQuery(array('query' => $Winners_JackPots));
        if($wj){
            $obj = json_decode($wj,true);
            $answers['topWinners'] = $obj['data']['topWinners'];
            $answers['jackpot'] = $obj['data']['jackpot'];
        }else{
            $answers['topWinners'] = false;
            $answers['jackpot'] = false;
        };

        return $answers;
    }

    public function GraphQl(){
        $answers = array();
        //get 9 popular games
        $answers['topGames'] = $this->popularGame();

        //get 5 winners and jackpot data
        $tj = $this->topWinnersJackpot();
        $answers['topWinners'] = $tj['topWinners'];
        $answers['jackpot'] = $tj['jackpot'];

        return $answers;
    }
}
