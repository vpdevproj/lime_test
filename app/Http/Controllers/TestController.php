<?php

namespace App\Http\Controllers;

use App\limeGraph;

class TestController extends Controller
{
   public function index(){
        $lime = new limeGraph();
        $data = $lime->GraphQl();
        return view('testtask',compact('data'));
   }
}
