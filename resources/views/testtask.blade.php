<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css"/>
    <link rel="stylesheet" href="/css/app.css"/>
    <title>Тестовое задание | Lime</title>
</head>
<body>
<!--HEADING-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <img src="https://limecs.co/wp-content/themes/limecs/img/lime.png" class="lime-logo"/>
                </div>
                <div class="col-md-9">
                    <h1>Тестовое задание<br/>Lime Consulting</h1>
                </div>
                <hr/>
            </div>
        </div>
    </div>
</section>
<!--JACKPOT-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="big red">ДЖЕКПОТ</span>
                <div class="bigredblock">
                    <span class="big numbers white">{{number_format((float)$data['jackpot']['amount'], 2, ',', ' ')}}{{($data['jackpot']['currency'] == 'USD')?'$':' '.$data['jackpot']['currency']}}</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- POPULAR GAMES -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Популярные игры ({{count($data['topGames'])}})</h2>
                @foreach($data['topGames'] as $game)
                    <div class="gameitem col-md-4">
                        <div class="gameimage">
                            <img src="{{$game['image']}}" class="img-responsive"/>
                        </div>
                        <div class="gameinfo">
                            <span><b>Название:</b> {{$game['fullGameName']}}</span>
                            <span><b>Игроков онлайн: </b> {{$game['playersOnline']}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!--TABLE TOP WINNERS-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Топ победителей ({{count($data['topWinners'])}})</h2>
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>№</th>
                        <th>Идентификатор пользователя</th>
                        <th>Логин</th>
                        <th>Игра</th>
                        <th>Дата выигрыша</th>
                        <th>Сумма выигрыша</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($data['topWinners'] as $t)
                        @php
                            $date = str_replace('T',' ',$t['date']);
                            $date = str_replace('Z','',$date);
                            $date = date('d.m.Y H:i:s',strtotime($date));
                        @endphp
                        <tr>
                            <th scope="row">{{$i}}</th>
                            <td>{{$t['uuid']}}</td>
                            <td>{{$t['username']}}</td>
                            <td>{{$t['game']['fullGameName']}}</td>
                            <td>{{$date}}</td>
                            <td>{{number_format((float)$t['winAmount']['amount'], 2, ',', ' ')}}{{($t['winAmount']['currency'] == 'USD')?'$':' '.$t['winAmount']['currency']}}</td>
                        </tr>
                        @php $i++; @endphp
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!--SECTION FOOTER-->
<section class="footer text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <small>Lime Consulting - TEST TASK &copy 2019 (26.01.2019)</small>
                <a href="/resume.pdf" target="_blank">Валерий Парубченко</a>
            </div>
        </div>
    </div>
</section>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>